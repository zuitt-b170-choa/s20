//Difference
/*
	1. Quotation Marks
		JS Objects only have the value inserted inside quotation marks
		JSON has the quotation marks for both key and the value

	2. 
		JS Objects - exclusive to javascript. Other programming languages cannot use JS object files
		JSON - not exclusive for javascript. Other programming languages can also use JSON files.
*/



/*let city = {
	city: "Ormoc",
	province: "Leyte",
	country: "Philippines"
}

console.log(city);*/

//JSON Object
/*
	JavaScript Object Notation
		used for serializing/deserializing different data types into bytes
		Serialization - process of converting data into bytes for easier transmission or transfer of information.
*/
/*let city = {
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
}

console.log(city);*/

/*let cities = [
{
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
},
{
	"city": "Bacoor",
	"province": "Cavite",
	"country": "Philippines"
},
{
	"city": "New York",
	"province": "New York",
	"country": "U.S.A"
}
]

console.log(cities);*/

//JSON METHODS

//	JSON object contains methods for parsing and converting data into stringified JSON
//	Stringifed JSON - a JSON object/JS objects converting into a string to be used in other functions of the language, especially Javascript-based applications (serialize)
let batches = [
{
	"batchName": "Batch X"

},
{
	"batchName": "Batch Y"

}
];

console.log("Result from console.log");
console.log(batches);

console.log("Result from console.log");
//stringify - used to convert JSON objects into JSON (string)*/
console.log(JSON.stringify(batches));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});

console.log(data);

/*var userDetails = {
	"fname": prompt("Please input your first name: "),
	"lname": prompt("Please input your last name: "),
	"age": prompt("Please input your age: "),
	"address": {
		"city": prompt("Please input your city: "),
		"country": prompt("Please input your country: "),
		"zipCode": prompt("Please input your zip code: ")
	}
};

console.log(JSON.stringify(userDetails));*/

// CONVERTING OF STRINGIFIED JSON INTO JS OBJECTS
	// Parse Method - converting JSON data into JS Objectes
	// Information is commonly sent to application in STRINGIFIED JSON; then converted into objects; this happens both for sending information to a backend app such as databases and back to frontend app such as the webpages.
	// upon receiving the data, JSON text can be converted back into a JS Object with parse method

let batchesJSON = `[
{
	"batchName": "Batch X"

},
{
	"batchName": "Batch Y"

}
]`;
console.log(batchesJSON);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedData = `{
	"name": "John",
	"age": 31,
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`;

console.log(JSON.parse(stringifiedData))
